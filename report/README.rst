::
  
  title: My super duper report
  author: FG-16-IU-000, Gregor von Laszewski, laszewski@gmail.com
  author: FG-16-IU-001, Author Two, author.two@email.com
  author: FG-16-IU-002, Author Three, three@email.com
  pages: 5
  status: completed/draft

::

  @TechReport{project-000, 
    author = 	 {von Laszewski, Gregor and Tw, Author and Three, Author},
    title = 	 {{Put Your Title Here}},
    institution =  {Indiana University},
    year = 	 2016,
    type = 	 {use Project or Paper},
    number = 	 {project-000},
    address = 	 {Bloomington, IN 47408, U.S.A.},
    month = 	 dec,
    url={put the pdf gitlab url here}
  }

MAKE SURE YOU PUT IN YOUR HID 